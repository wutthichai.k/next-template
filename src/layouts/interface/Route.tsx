import { MenuDataItem } from "./MenuDataItem";

export interface Route extends MenuDataItem {
    routes?: Route[];
}