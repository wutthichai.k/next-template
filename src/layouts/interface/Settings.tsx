export interface Settings {
  /** Theme for nav menu */
  navTheme: "light" | "dark";
  /** Primary color of ant design */
  primaryColor: string;
  /** Nav menu position: `side` or `top` */
  layout: "side" | "top";
  /** Layout of content: `Fluid` or `Fixed`, only works when layout is top */
  contentWidth: "Fluid" | "Fixed";
  /** Sticky header */
  fixedHeader: boolean;
  /** Sticky siderbar */
  fixSiderbar: boolean;
  menu: { locale: boolean };
  title: string;
  pwa: boolean;
  // Your custom iconfont Symbol script Url
  // eg：//at.alicdn.com/t/font_1039637_btcrd5co4w.js
  // Usage: https://github.com/ant-design/ant-design-pro/pull/3517
  iconfontUrl: string;
  colorWeak: boolean;
}
