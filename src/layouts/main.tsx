import dynamic from "next/dynamic";
import Link from "next/link";
import { useState } from "react";

import { MenuDataItem } from "./interface/MenuDataItem";
import { ROUTES } from "./route";
import { Settings } from "./interface/Settings";

const ProLayout = dynamic(() => import("@ant-design/pro-layout"), {
  ssr: false,
});

const SettingDrawer = dynamic(
  () => import("@ant-design/pro-layout").then((module) => module.SettingDrawer),
  {
    ssr: false,
  }
);

export default function Main({ children }) {
  const [settings, setSettings] = useState({});

  console.log(SettingDrawer);

  return (
    <div id="pro-layout">
      <ProLayout
        {...settings}
        style={{ minHeight: "100vh" }}
        route={ROUTES}
        menuItemRender={(options: MenuDataItem, element: React.ReactNode) => (
          <Link href={options.path}>
            <a>{element}</a>
          </Link>
        )}
      >
        {children}
      </ProLayout>
      <SettingDrawer settings={settings} onSettingChange={setSettings} />
    </div>
  );
}
