import {
  SmileOutlined,
  SettingOutlined,
  PlaySquareOutlined,
} from "@ant-design/icons";

import { Route } from './interface/Route'

export const ROUTES: Route = {
  path: "/",
  routes: [
    {
      path: "/",
      name: "Welcome",
      icon: <SmileOutlined />,
      routes: [
        {
          path: "/welcome",
          name: "Account Settings",
          icon: <SettingOutlined />,
        },
      ],
    },
    {
      path: "/example",
      name: "Example Page",
      icon: <PlaySquareOutlined />,
    },
  ],
};
