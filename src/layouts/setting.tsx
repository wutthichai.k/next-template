import { ProSettings } from "@ant-design/pro-layout";

export const Settings: ProSettings = {
  navTheme: "light",
  layout: "top",
  contentWidth: "Fixed",
  fixedHeader: false,
  fixSiderbar: false,
  menu: {
    locale: true,
  },
  headerHeight: 48,
  title: "Ant Design Pro",
  iconfontUrl: "",
  primaryColor: "#1890ff",
  splitMenus: false,
};
~